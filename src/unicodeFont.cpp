#include "unicodeFont.h"

void unicodeFont::readFont(uint16_t unicode){
    uint32_t Address = 0;
    bool isSymbol = false;

    if(unicode >= 0x4E00 && unicode <= 0x9FA5){
        Address = (unicode - 0x4E00) * 32; //基本汉字:
    }
    else if(unicode >= 0x9FA6 && unicode <= 0x9FD5){
        Address = (unicode - 0x9FA6 + 20902) * 32; //基本汉字补充
    }
    else if(unicode >= 0x3400 && unicode <= 0x4DB5){
        Address = (unicode - 0x3400 + 20950) * 32; //扩展A
    }
    else if(unicode >= 0xF900 && unicode <= 0xFAD9){
        Address = (unicode - 0xF900 + 27532) * 32; //兼容汉字
    }
    else if(unicode >= 0x3000  && unicode <= 0x303F){
        Address = (unicode - 0x3000 + 28006) * 32; //CJK标点符号
    }
    else if(unicode >= 0xFF00 && unicode <= 0xFFEE){
        Address = (unicode - 0xFF00 + 28070) * 32; //半角及全角字符
    }
    else if(unicode >= 0xFE10 && unicode <= 0xFE19){
        Address = (unicode - 0xFE10 + 28309) * 32; //CJK纵向标点符号
    }
    else if(unicode >= 0xFE30 && unicode <= 0xFE4F){
        Address = (unicode - 0xFE30 + 28319) * 32; //CJK特殊符号(月份, 日期)
    }
    else if(unicode >= 0x3200 && unicode <= 0x33FF){
        Address = (unicode - 0x3200 + 28351) * 32; //
    }
    else if(unicode >= 0x31C0 && unicode <= 0x31E3){
        Address = (unicode - 0x31C0 + 28863) * 32; //汉字笔画
    }
    else if(unicode >= 0x2F00 && unicode <= 0x2FD5){
        Address = (unicode - 0x2F00 + 28899) * 32; //康熙部首
    }
    else if(unicode >= 0x2000 && unicode <= 0x2FFF){
        Address = (unicode - 0x2000) * 32;
        isSymbol = true;
    }
    else{
        Address = (28070 + 0x1F) * 32; //问号
    }
    if(Address >= bin_dataLength) return;
    uint16_t temp[16] ={0};
    if(isSymbol){
        for(int a = 0, c = 0; a < 16; a++,c+=2){
            temp[a] = (pgm_read_byte(symbol_data + Address + c) << 8) + pgm_read_byte(symbol_data + Address + c + 1);
        }
    }else{
        for(int a = 0, c = 0; a < 16; a++,c+=2){
            temp[a] = (pgm_read_byte(bin_data + Address + c) << 8) + pgm_read_byte(bin_data + Address + c + 1);
        }
    }
    for(int i = 0; i < 16; i++){
        uint16_t data = 0;
        for(int j = 15; j >= 0; j--){
            data <<= 1;
            data += (temp[j] >> (15 - i)) & 1;
        }
        fontData[i] = data;
    }
}

void unicodeFont::readFontHalf(uint16_t unicode){
    uint32_t Address = 0;
    int BaseAdd = 0;
    if( unicode >= 0x20 && unicode <= 0xe7f )//latin 1
        Address = 16*(unicode-0x20)+ 0;
    // else if( unicode >= 0xa0 && unicode <= 0xff )//latin 2
    //     Address = 16*(unicode-0xa0)+ 96;
    // else if( unicode >= 0x100 && unicode <= 0x17f )//latin 3
    //     Address = 16*(unicode-0x100)+ 192;
    // else if( unicode >= 0x1a0 && unicode <= 0x1cf )//latin 4
    //     Address = 16*(unicode-0x1a0)+ 320;
    // else if( unicode >= 0x1f0 && unicode <= 0x1ff )//latin 5
    //     Address = 16*(unicode-0x1f0)+ 368;
    // else if( unicode >= 0x210 && unicode <= 0x21f )//latin 6
    //     Address = 16*(unicode-0x210)+ 384;
    // else if( unicode >= 0x1ea0 && unicode <= 0x1eff )//latin 7
    //     Address = 16*(unicode-0x1ea0)+ 400;
    // else if( unicode >= 0x370 && unicode <= 0x3cf )//greek
    //     Address = 16*(unicode-0x370)+ 496;
    // else if( unicode >= 0x400 && unicode <= 0x45f )//cyrillic 1
    //     Address = 16*(unicode-0x400)+ 592;
    // else if( unicode >= 0x490 && unicode <= 0x4ff )//cyrillic 2
    //     Address = 16*(unicode-0x490)+ 688;
    // else if( unicode >= 0x590 && unicode <= 0x5ff )//hebrew
    //     Address = 16*(unicode-0x590)+ 800;
    // else if( unicode >= 0xe00 && unicode <= 0xe7f )//thai
    //     Address = 16*(unicode-0xe00)+ 912;
    else
        Address = BaseAdd;
    if(Address >= bin_data_halfLength) return;
    uint8_t temp[16] ={0};
    for(int a = 0; a < 16; a++){
        temp[a] = (pgm_read_byte(bin_data_half + Address + a));
    }
    for(int i = 0; i < 8; i++){
        uint16_t data = 0;
        for(int j = 15; j >= 0; j--){
            data <<= 1;
            data += (temp[j] >> (7 - i)) & 1;
        }
        fontData_half[i] = data;
    }
}