#ifndef GT20L16P1Y_H
#define GT20L16P1Y_H


#define FONT_MOSI 4
#define FONT_MISO 10
#define FONT_SCL 3
#define FONT_CS 11 //原为 0

#include <SPI.h>
#include <Arduino.h>
#include <stdint.h>
#include <string.h>

class GT20L16P1Y{
public :

    uint8_t _matrixdata32[32]; //16×16
    uint8_t _matrixdata16[16]; //16×8
    uint8_t _matrixdata16_deg90[16];
    uint16_t _matrixdata32_deg90[32];
    uint16_t fixData_16[8];
    uint16_t fixData_32[16];
    SPISettings _settings;

    void begin();
    void fix_16bit();
    void fix_32bit();
    void deg270_16bit();
    void deg270_32bit();
    void readFont8x16(uint16_t ASCIICODE);
    void readFont16x16(uint16_t UNICODE);
    void readFontGB2312(uint16_t GB2312CODE);
    int utf8ToUnicode(const char* utf8, int* unicode);
    int utf8StringToUnicode(const char* utf8, int* unicode);
    void utf8ChineseToUnicode(const char* utf8, unsigned short* unicode);
    void utf8ToUnicode2Bytes(const char* utf8, unsigned char* unicode);
};

#endif // GT20L16P1Y_H