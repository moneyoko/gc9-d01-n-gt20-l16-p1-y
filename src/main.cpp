#include "TFT_GC9D01N.h"
#include "GT20L16P1Y.h"
#include "FFat.h"

TFT_GC9D01N_Class TFT_099;
GT20L16P1Y zh_FONT;
char *str = "0123 world ABC";
char *str2 = "4567 hello EFG";
char *str3 = "你好";
unsigned char toOrd(char ch)
{
    if (ch < 32)
    {
        ch = 95;
    }
    else if ((ch >= 32) && (ch <= 47))
    {
        ch = (ch - 32) + 10 + 62;
    }
    else if ((ch >= 48) && (ch <= 57))
    {
        ch = ch - 48;
    }
    else if ((ch >= 58) && (ch <= 64))
    {
        ch = (ch - 58) + 10 + 62 + 16;
    }
    else if ((ch >= 65) && (ch <= 126))
    {
        ch = (ch - 65) + 10;
    }
    else if (ch > 126)
    {
        ch = 95;
    }

    return ch;
}

void drawChar(int unicode)
{

    if (unicode <= 0x1EFF)
    {
        zh_FONT.readFont8x16(unicode);
        for (int a = 0; a < 8; a++)
        {
            uint16_t dat = zh_FONT.fixData_16[a];
            // Serial.print(dat, HEX);
            // Serial.print(" ");
            for (int b = 0; b < 16; b++)
            {
                Serial.print(dat & 0x8000 ? "● " : "  ");
                dat <<= 1;
            }
            Serial.println();
        }
    }
    else
    {
        //     unicode -= 0x4E00;
        //     int startPos = unicode * 32;
        //     for(int a = startPos; a < 32; a+=2){
        //         uint16_t dat = bin_data[a] << 8 + bin_data[a + 1];
        //         // Serial.print(dat, HEX);
        //         // Serial.print(" ");
        //         for(int b = 0; b < 16; b++){
        //             Serial.print(dat & 0x8000 ? "● " : "  ");
        //             dat <<= 1;
        //         }
        //         Serial.println();
        //     }
    }

    Serial.println();
    Serial.println("**********************************************");
}

void setup()
{
    // put your setup code here, to run once:
    Serial.begin(115200);
    delay(5000);
    Serial.println("setup ");
    if(!FFat.begin(true)){
        Serial.println("An Error has occurred while mounting FFat");
        return;
    }
    Serial.printf("Total space: %10lu\n", FFat.totalBytes());
    Serial.printf("Free space:  %10lu\n\n", FFat.freeBytes());

    TFT_099.begin();
    TFT_099.backlight(50);

    TFT_099.DispColor(0, 0, TFT_WIDTH, TFT_HEIGHT, BLACK);
    delay(500);

    TFT_099.print("爱意随风起,", 2, 5, WHITE, BLACK);
    TFT_099.print("风止意难平", 2, 23, WHITE, BLACK);
    Serial.println();
}
int i = 0xB0A0;
void loop()
{
    if (Serial.available() > 0)
    {
        Serial.read();
        String str = "1、横眉冷对千夫指，俯首干为孺子牛。\
        2、时间就是生命，无端地空耗别人的时间，无异于谋财害命。\
        3、幼稚对于老成，有如孩子对于老人，决没有什么耻辱的，作品也一样，起初幼稚，不算耻辱的。\
        4、我自爱我的野草，但我憎恶这以野草作装饰的地面。\
        5、我好像是一只牛，吃的是草，挤出的是奶、血。\
        6、贪安稳就没有自由，要自由就要历些危险。只有这两条路。\
        7、凡是总须研究，才会明白。\
        8、我们中国人对于不是自己的东西，或者将不为自己所有的东西，总要破坏了才快活的。\
        9、与名流者谈，对于他之所讲，当装作偶有不懂之处。太不懂被看轻，太懂了被厌恶。偶有不懂之处，彼此最为合宜。 ";
        int len = 0;
        int showLen = 0;
        int addr[100] = {0};
        for (int i = 0; i < 100; i++)
        {
            addr[i] = len;
            int result = TFT_099.calculateLength((char *)str.c_str() + len, 0, 1);
            if (result == 0)
                break;
            len += result;
            showLen = i;
            Serial.println("len:" + String(len));
            Serial.println("strLength:" + String(str.length()));
            Serial.println("return:" + str.substring(len));
        }
        for (int a = 0; a <= showLen; a++)
        {
            delay(2000);
            TFT_099.DispColor(0, 0, TFT_WIDTH, TFT_HEIGHT, BLACK);
            len = TFT_099.print((char *)str.c_str() + addr[a], 0, 1, WHITE, BLACK);
            Serial.println("len:" + String(len));
            Serial.println("strLength:" + String(str.length()));
            Serial.println("return:" + str.substring(len));
        }
        for (int a = showLen; a >= 0; a--)
        {
            delay(2000);
            TFT_099.DispColor(0, 0, TFT_WIDTH, TFT_HEIGHT, BLACK);
            len = TFT_099.print((char *)str.c_str() + addr[a], 0, 1, WHITE, BLACK);
            Serial.println("len:" + String(len));
            Serial.println("strLength:" + String(str.length()));
            Serial.println("return:" + str.substring(len));
        }
    }
}